;(function(){
	var controller = new ScrollMagic.Controller();

	// animations
	var callToActionSpans = new TimelineMax()
		.add(TweenMax.fromTo(".call-to-action", 200, {backgroundPosition: "-100vw"}, {backgroundPosition: "0vw"}))
		.add(TweenMax.staggerFromTo(".call-to-action__title span", 100, {left: -1700}, {left: 0}, 30));

	var questions = new TimelineMax()
		.add(TweenMax.staggerTo(".questions__item", 1, {display: "block", opacity: 1}, 10))
		.add(TweenMax.staggerTo(".questions__item", 3, {display: "none", opacity: 0}, 10), 5);

	// Scenes for .questions and .call-to-action
	new ScrollMagic.Scene({
		triggerElement: '.questions',
		triggerHook: 0,
		duration: "250%"
	})
	.setPin(".questions")
	.setTween(questions)
	.addTo(controller);

	new ScrollMagic.Scene({
		triggerElement: '.call-to-action',
		triggerHook: 0,
		duration: "150%"
	})
	.setPin(".call-to-action")
	.setTween(callToActionSpans)
	.addTo(controller);
})();