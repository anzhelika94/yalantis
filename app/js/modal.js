;
$(document).ready(function(){
	$('[data-get-modal]').click(function(){
		var modalIndent = $(this).attr('data-get-modal');
		$('.modal[data-modal=' + modalIndent +']').css('display','flex');
	});
	$('.modal__close').click(function(){
		$(this).closest('.modal').css('display', 'none');
	});

	TweenLite.set(".modal-inner", {transformStyle:"preserve-3d"});
	TweenLite.set(".modal-back", {rotationY:-180});
	TweenLite.set([".modal-back", ".modal-front"], {backfaceVisibility:"hidden"});

	$(".js-flip-form").click(function() {
		$(this).closest(".modal").attr('flip-active', 'true');
		TweenLite.to($(this).closest(".modal-inner"), 1.2, {rotationY:180, ease:Back.easeOut});
	}

	// function() {
	//   TweenLite.to($(this).find(".modal-inner"), 1.2, {rotationY:0, ease:Back.easeOut});  
	// }
	);
});